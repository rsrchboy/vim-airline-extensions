" Title:  Airline extension to display HEAD's subject line
" Author: Chris Weyl <cweyl@alumni.drew.edu>

" parts cribbed heavily from airline's extensions/example.vim

" Due to some potential rendering issues, the use of the `space` variable is
" recommended.
let s:spc = g:airline_symbols.space

" First we define an init function that will be invoked from extensions.vim
function! airline#extensions#gitsubject#init(ext)

    " Here we define a new part for the plugin.  This allows users to place this
    " extension in arbitrary locations.
    call airline#parts#define_raw('head', '%{airline#extensions#gitsubject#get_head()}')

    " Next up we add a funcref so that we can run some code prior to the
    " statusline getting modifed.
    call a:ext.add_statusline_func('airline#extensions#gitsubject#apply')

    " You can also add a funcref for inactive statuslines.
    " call a:ext.add_inactive_statusline_func('airline#extensions#gitsubject#unapply')

    return
endfunction

" This function will be invoked just prior to the statusline getting modified.
function! airline#extensions#gitsubject#apply(...)
    " Let's say we want to append to section_c, first we check if there's
    " already a window-local override, and if not, create it off of the global
    " section_c.
    let w:airline_section_c = get(w:, 'airline_section_c', g:airline_section_c)

    " FIXME no separator needed if we don't have a status
    " Then we just append this extenion to it, optionally using separators.
    let l:text = airline#extensions#gitsubject#status()
    let w:airline_section_c .= g:airline_left_alt_sep . s:spc . l:text

    return
endfunction

let s:_bold = '%{#__accent_bold#}'
let s:_red = '%#__accent_red#'
let s:_yellow = '%#__accent_yellow#'
let s:_purple = '%#__accent_purple#'
let s:_blue = '%#__accent_blue#'
let s:_italic = '%#__accent_italic#'
let s:_orange = '%#__accent_orange#'
let s:_restore = '%#__restore__#'

" Finally, this function will be invoked from the statusline.
function! airline#extensions#gitsubject#status()

    if exists('b:airlinex_gitsubject') | return b:airlinex_gitsubject | endif

    if &previewwindow || &diff || (@% =~# '^fugitive://.*') || ( &buftype ==# 'quickfix' )
        let b:airlinex_gitsubject = ''
        return ''
    endif

    if get(b:, 'git_dir', "") != ""
        try
            let b:airlinex_gitsubject = ducttape#git#gitsubject#subject()
        catch /^Vim\%((\a\+)\)\=:E117/
            let b:airlinex_gitsubject = FugitiveExecute('log', '-1', '--pretty=%s', '--no-show-signature').stdout[0]
            let b:dt_error_airline_gitsubject = v:throwpoint . ' -- ' . v:exception
        endtry
    else
        let b:airlinex_gitsubject = ''
    endif

    let b:airlinex_gitsubject = substitute(b:airlinex_gitsubject, '\%(\(fixup\|squash\)! \)\+', s:_blue .. '\1!' .. s:_restore .. ' ', '')

    return b:airlinex_gitsubject
endfunction
"
" au's to force a re-read of the subject
augroup airline#extensions#gitsubject
    au!

    autocmd BufEnter,FileChangedShellPost,ShellCmdPost,CmdwinLeave * unlet! b:airlinex_gitsubject
    autocmd User FugitiveCommitFinishPre unlet! b:airlinex_gitsubject
    autocmd User FugitiveChanged unlet! b:airlinex_gitsubject
augroup END
