scriptencoding utf-8

let s:spc = g:airline_symbols.space

if !exists('g:airline#extensions#ssh#enable')
  let g:airline#extensions#ssh#enable = 1
endif

if !exists('g:airline#extensions#ssh#enable_built_in')
  let g:airline#extensions#ssh#enable_built_in = 1
endif

if !exists('g:airline#extensions#ssh#text')
  let g:airline#extensions#ssh#text =  $USER . '@' . hostname()
endif

function! airline#extensions#ssh#init(ext)

  call airline#parts#define_raw('ssh', '%{airline#extensions#ssh#snippet()}')
  call airline#parts#define_condition('ssh', '$SSH_CLIENT != ""')

  " called before the statusline is modifed
  call a:ext.add_statusline_func('airline#extensions#ssh#apply')

  " You can also add a funcref for inactive statuslines.
  " call a:ext.add_inactive_statusline_func('airline#extensions#ssh#unapply')
endfunction

function! airline#extensions#ssh#apply(...)

    let l:text = airline#extensions#ssh#snippet()

    " don't bother if unnecessary
    if l:text == "" || g:airline#extensions#ssh#enable_built_in != 1
        return
    endif


    let w:airline_section_c = get(w:, 'airline_section_c', g:airline_section_c)
    let w:airline_section_c =
        \ l:text
        \ . s:spc
        \ . g:airline_left_alt_sep
        \ . s:spc
        \ . w:airline_section_c

    return
endfunction

function! airline#extensions#ssh#snippet()

    " don't bother if unnecessary
    if $SSH_CLIENT == "" || g:airline#extensions#ssh#enable != 1
        return
    endif

    return '%#__accent_red#'
        \ . g:airline#extensions#ssh#text
        \ . '%#__restore__#'
endfunction
