# vim-airline-extensions

This is a collection of extensions for
[vim-airline](autoload/airline/extensions/tablemode.vim).  I've been keeping
them in my `~/.vim/`, but heck, maybe they'll be useful to someone else, too.
They try to be quiet when not needed, but to provide information that's either
clueful (e.g. subject line of the last git commit) or potentially surprising
(wait, you mean I'm not editing this file on my laptop?!).

There's not much in the way of documentation, but the actual extensions aren't
that long.  I've tried to keep any interesting variables up towards the top.

I find these useful, but as with everything, YMMV.

## extension: ssh-client

Ever start editing a file, only to forget you're on a different host?  Yeah,
me neither, but I've heard some stories.  This extension checks to see if
`$SSH_CLIENT` is set, and if so, displays the remote user and hostname before
the filename in the status line.

### extension: gitsubject

Appends the subject of the HEAD commit (that is, the tip of the current
branch) to section c (generally, right after the file name).

# LICENSE

This software is Copyright 2021 Chris Weyl <cweyl@alumni.drew.edu>.  It is
licensed under the LGPL v3.0; see the file `LICENSE` in this repository.
